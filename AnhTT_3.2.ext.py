decimal_num = 1234567899999999999999999999999999
temp = decimal_num
hex_num = ''
hex_range = range(10,16)
hex_words = ['A', 'B', 'C', 'D', 'E', 'F']
hex_items = zip(hex_range, hex_words)
while temp > 0:
    remainder = temp % 16
    temp /= 16
    if remainder in hex_range:
        for i in hex_items:
            if i[0] == remainder:
                hex_num += i[1]
    else:
        hex_num += str(remainder)

print decimal_num, '<=>', hex_num[::-1]